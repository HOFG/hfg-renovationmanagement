﻿using EllieMae.Encompass.Forms;
using System;
using winform = System.Windows.Forms;
using EllieMae.Encompass.Automation;
using EllieMae.Encompass.BusinessObjects.Loans;
using EllieMae.Encompass.BusinessObjects.Loans.Logging;
using EllieMae.Encompass.Collections;
using HFG_RenovationManagement.GlobalProperties;
namespace HFG_RenovationManagement
{
    public class InputForm : Form
    {
        decimal IDRB = 0;
        decimal RRF = 0;
        decimal FRBUD = 0;
        internal CheckBox CBPERMIT = null;
        internal CheckBox CBAF = null;
        internal CheckBox CBHCF = null;

        internal TextBox TextBox188 = null;
        internal TextBox TextBox128 = null;
        internal TextBox TextBox129 = null;
        internal TextBox TextBox130 = null;
        internal TextBox TextBox131 = null;
        internal TextBox TextBox132 = null;
        internal TextBox TextBox133 = null;
        internal TextBox TextBox134 = null;
        internal TextBox TextBox135 = null;
        internal TextBox TextBox136 = null;
        internal TextBox TextBox137 = null;
        internal TextBox TextBox194 = null;
        internal TextBox TextBox206 = null;

        internal TextBox TextBox190 = null;
        internal TextBox TextBox148 = null;
        internal TextBox TextBox150 = null;
        internal TextBox TextBox152 = null;
        internal TextBox TextBox154 = null;
        internal TextBox TextBox156 = null;
        internal TextBox TextBox158 = null;
        internal TextBox TextBox160 = null;
        internal TextBox TextBox162 = null;
        internal TextBox TextBox164 = null;
        internal TextBox TextBox166 = null;
        internal TextBox TextBox196 = null;
        internal TextBox TextBox208 = null;
        internal DropdownBox DropdownBox7;
        internal DropdownBox DropdownBox8;
        internal DropdownBox DropdownBox9;
        internal DropdownBox DropdownBox10;

        internal TextBox TextBox101 = null;
        internal TextBox TextBox103 = null;
        internal TextBox TextBox104 = null;
        internal TextBox TextBox110 = null;
        internal TextBox TextBox113 = null;

        public InputForm()
        {
            this.Load += new EventHandler(Form_load);

        }
        private void Form_load(object sender, EventArgs e)
        {
            EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].Value = "0.0005";
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.C1.INITIALDRAW"].Value) > 0)
            {
                IDRB = IDRB + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.C1.INITIALDRAW"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.C2.INITIALDRAW"].Value) > 0)
            {
                IDRB = IDRB + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.C2.INITIALDRAW"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.C3.INITIALDRAW"].Value) > 0)
            {
                IDRB = IDRB + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.C3.INITIALDRAW"].Value);
            }

            EncompassApplication.CurrentLoan.Fields["CX.RM.RENOINITIALDRAW"].Value = IDRB.ToString();
            
            CalculateSectionBRenovationBudgetDetails();


            SetFMNAFHACheckBox();
            SetC5A5D5();

            CalculateA14();
            CalculateB14();
            CalculateD14();
        }

        

        private void CalculateFHA()
        {
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X103"].Value) > 0)
            {
                //RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X103"].Value);
                TextBox101.Text = EncompassApplication.CurrentLoan.Fields["MAX23K.X103"].Value.ToString();

            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X80"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X80"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X10"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X10"].Value);
                TextBox103.Text = EncompassApplication.CurrentLoan.Fields["MAX23K.X10"].Value.ToString();
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X95"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X95"].Value);
                TextBox104.Text = EncompassApplication.CurrentLoan.Fields["MAX23K.X95"].Value.ToString();
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X113"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X113"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X114"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X114"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X44"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X44"].Value);
                TextBox110.Text = EncompassApplication.CurrentLoan.Fields["MAX23K.X44"].Value.ToString();
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOINITIALDRAW"].Value) > 0)
            {
                RRF = RRF - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOINITIALDRAW"].Value);
            }
            EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value = RRF.ToString();
            


            if (CBAF.Checked)
            {
                //FRBUD = FRBUD - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value);
                RRF = RRF - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value);
                EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value = RRF.ToString();
                // EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAFINALBUDGET"].Value = FRBUD.ToString();
            }
            if (CBHCF.Checked)
            {
                //FRBUD = FRBUD - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value);
                RRF = RRF - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value);
                EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value = RRF.ToString();
                //EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAFINALBUDGET"].Value = FRBUD.ToString();
            }
            if (CBPERMIT.Checked)
            {
                //FRBUD = FRBUD - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value);
                RRF = RRF - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value);
                EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value = RRF.ToString();
                //EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAFINALBUDGET"].Value = FRBUD.ToString();
            }

            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X17"].Value) > 0)
            {
                FRBUD = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X17"].Value);
                TextBox113.Text = EncompassApplication.CurrentLoan.Fields["MAX23K.X17"].Value.ToString();
            }
            else
            {
                FRBUD = RRF;
            }

            //EncompassApplication.CurrentLoan.Fields["CX.RM.RECON.MOR.AMT"].Value = FRBUD.ToString();
            //TextBox113.Text = FRBUD.ToString();

            EncompassApplication.CurrentLoan.Fields["CX.RM.RECON.MOR.AMT"].Value = EncompassApplication.CurrentLoan.Fields["MAX23K.X17"].FormattedValue;
            TextBox113.Text = EncompassApplication.CurrentLoan.Fields["MAX23K.X17"].FormattedValue;
            TextBox190.Text = EncompassApplication.CurrentLoan.Fields["MAX23K.X17"].FormattedValue;
            decimal remainingFunds = 0;
            decimal renopayers = 0;
            if (EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].FormattedValue != "")
            {
                remainingFunds = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].FormattedValue);
            }
            if (EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].FormattedValue != "")
            {
                renopayers = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X17"].FormattedValue);
            }
            EncompassApplication.CurrentLoan.Fields["CX.RM.RECON.MOR.AMT"].Value = (remainingFunds + renopayers);
        }

        private void CalculateConventional()
        {
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.ESCROWTOTAL"].Value) > 0)
            {
                // RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.ESCROWTOTAL"].Value);
                TextBox101.Text = EncompassApplication.CurrentLoan.Fields["CX.REN.ESCROWTOTAL"].Value.ToString();
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X80"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X80"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].Value);
                TextBox103.Text = EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOCONTRES"].Value.ToString();
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].Value);
                TextBox104.Text = EncompassApplication.CurrentLoan.Fields["CX.REN.BOROWNFUNDS"].Value.ToString();
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X113"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X113"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X114"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X114"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value);
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOOTHER"].Value) > 0)
            {
                RRF = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOOTHER"].Value);
                TextBox110.Text = EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOOTHER"].Value.ToString();
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOINITIALDRAW"].Value) > 0)
            {
                RRF = RRF - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOINITIALDRAW"].Value);
            }
            EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value = RRF.ToString();
            TextBox101.Text = RRF.ToString();
            EncompassApplication.CurrentLoan.Fields["CX.REN.ESCROWTOTAL"].Value = RRF.ToString();
            if (CBAF.Checked)
            {
                //FRBUD = FRBUD - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value);
                RRF = RRF - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X19"].Value);
                EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value = RRF.ToString();
                // EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAFINALBUDGET"].Value = FRBUD.ToString();
            }
            if (CBHCF.Checked)
            {
                //FRBUD = FRBUD - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value);
                RRF = RRF - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X20"].Value);
                EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value = RRF.ToString();
                //EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAFINALBUDGET"].Value = FRBUD.ToString();
            }
            if (CBPERMIT.Checked)
            {
                //FRBUD = FRBUD - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value);
                RRF = RRF - Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["MAX23K.X21"].Value);
                EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value = RRF.ToString();
                //EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAFINALBUDGET"].Value = FRBUD.ToString();
            }
            if (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].Value) > 0)
            {
                FRBUD = RRF + Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].Value);
                TextBox113.Text = EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].Value.ToString();
            }
            else
            {
                FRBUD = RRF;
            }
            TextBox190.Text = EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].FormattedValue;
            
            TextBox113.Text = EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].FormattedValue;
            decimal remainingFunds = 0;
            decimal renopayers = 0;
            if(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].FormattedValue != "")
            {
                remainingFunds = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].FormattedValue);
            }
            if(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].FormattedValue != "")
            {
                renopayers = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.EHD.RENOPAYRES"].FormattedValue);
            }
            EncompassApplication.CurrentLoan.Fields["CX.RM.RECON.MOR.AMT"].Value = (remainingFunds + renopayers);
        }

        private void CalculateSectionBRenovationBudgetDetails()
        {
            if (EncompassApplication.CurrentLoan.Fields["1172"].Value.ToString() == "FHA")
            {
                CalculateFHA();
            }
            else
            {
                CalculateConventional();
            }
        }

        private void CalculateD14()
        {
            decimal D1 = 0;
            if(TextBox190.Text !="")
            {
                D1 = Convert.ToDecimal(TextBox190.Text);
            }
            else
            {
                D1 = 0;
            }
             
            var D14 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D2"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D3"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D4"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D5"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D6"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D7"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D8"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D9"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D10"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D11"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D12"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D13"].Value) +
                    D1;
            EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.d14"].Value = D14.ToString();
        }

        private void CalculateB14()
        {
            if (EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.FHA"].Value.ToString() == "X")
            {
                var B14 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A2"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A3"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A4"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A5"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A6"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A7"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A8"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A9"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A10"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A11"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A12"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A13"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value);
                EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.B14"].Value = B14.ToString();
            }
        }

        private void CalculateA14()
        {
            if (EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.FNMA"].Value.ToString() == "X")
            {
                var A14 = Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A2"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A3"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A4"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A5"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A6"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A7"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A8"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A9"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A10"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A11"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A12"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A13"].Value) +
                    Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value);
                EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A14"].Value = A14.ToString();
            }
        }

        private void SetC5A5D5()
        {

            if (EncompassApplication.CurrentLoan.Fields["1999"].FormattedValue != "//")
            {

                DateTime date1999 = Convert.ToDateTime(EncompassApplication.CurrentLoan.Fields["1999"].FormattedValue);

                DateTime lastBusinessDay = new DateTime();
                var i = DateTime.DaysInMonth(date1999.Year, date1999.Month);
                var dtCurrent = new DateTime();
                while (i > 0)
                {
                    dtCurrent = new DateTime(date1999.Year, date1999.Month, i);
                    if (dtCurrent.DayOfWeek < DayOfWeek.Saturday && dtCurrent.DayOfWeek > DayOfWeek.Sunday)
                    {
                        lastBusinessDay = dtCurrent;
                        i = 0;
                    }
                    else
                    {
                        i = i - 1;
                    }
                }

                EncompassApplication.CurrentLoan.Fields["CX.RM.ED.FIRSTPARMONTH"].Value = dtCurrent.ToString();
                var days1999 = date1999.Day;
                var curdays = dtCurrent.Day;
                var diff = curdays - days1999;

                if (EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].FormattedValue == "")
                {
                    EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value = "0.00";
                }
                if (EncompassApplication.CurrentLoan.Fields["CX.RM.RECON.MOR.AMT"].FormattedValue == "")
                {
                    EncompassApplication.CurrentLoan.Fields["CX.RM.RECON.MOR.AMT"].Value = "0.00";
                }

                var a5 = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].Value) / 365) *
                    (diff * (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RENOVAREMAININGFUNDS"].Value)));
                var d5 = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].Value) / 365) *
                    (diff * (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.RECON.MOR.AMT"].Value)));
                EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A5"].Value = a5;
                EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D5"].Value = d5;
            }

        }

        private void SetFMNAFHACheckBox()
        {

            if (EncompassApplication.CurrentLoan.Fields["1401"].FormattedValue.Contains("RENO"))
            {
                //A15 Field Value 
                

                if (EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTHS15FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTHS30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTHSHB15FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTHSHB30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTHSHR30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLHS15FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLHS30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLHSHB15FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLHSHB30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLHSHR30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLHSHB30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLHSHR30FRENO")
                {
                    EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.FNMA"].Value = "X";
                }
                else
                {
                    EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.FNMA"].Value = "";
                }


                if (EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTF203k$100LTD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTF203k$100STD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTF203kHBLTD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTF203kHBSTD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTF203kLTD15FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTF203kLTD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTF203kSTD15FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "MTF203kSTD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLF203k$100LTD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLF203k$100STD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLF203kHBLTD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLF203k$100LTD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLF203k$100STD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLF203kSTD30FRENO" ||
                    EncompassApplication.CurrentLoan.Fields["1401"].Value.ToString() == "PLF203kLTD30FRENO")
                {

                    EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.FHA"].Value = "X";
                }
                else
                {
                    EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.FHA"].Value = "";
                }

            }
        }

        public override void CreateControls()
        {
            CBAF = FindControl(FC.ControlIds.CBAF) as CheckBox;
            CBHCF = FindControl(FC.ControlIds.CBHCF) as CheckBox;
            CBPERMIT = FindControl(FC.ControlIds.CBPERMIT) as CheckBox;
            TextBox188 = FindControl(FC.ControlIds.TextBox188) as TextBox;
            TextBox188.Change += new EventHandler(txt188_Change);
            TextBox128 = FindControl(FC.ControlIds.TextBox128) as TextBox;
            TextBox128.Change += new EventHandler(txt188_Change);
            TextBox129 = FindControl(FC.ControlIds.TextBox129) as TextBox;
            TextBox129.Change += new EventHandler(txt188_Change);
            TextBox130 = FindControl(FC.ControlIds.TextBox130) as TextBox;
            TextBox130.Change += new EventHandler(txt188_Change);
            TextBox131 = FindControl(FC.ControlIds.TextBox131) as TextBox;
            TextBox131.Change += new EventHandler(txt188_Change);
            TextBox132 = FindControl(FC.ControlIds.TextBox132) as TextBox;
            TextBox132.Change += new EventHandler(txt188_Change);
            TextBox133 = FindControl(FC.ControlIds.TextBox133) as TextBox;
            TextBox133.Change += new EventHandler(txt188_Change);
            TextBox134 = FindControl(FC.ControlIds.TextBox134) as TextBox;
            TextBox134.Change += new EventHandler(txt188_Change);
            TextBox135 = FindControl(FC.ControlIds.TextBox135) as TextBox;
            TextBox135.Change += new EventHandler(txt188_Change);
            TextBox136 = FindControl(FC.ControlIds.TextBox136) as TextBox;
            TextBox136.Change += new EventHandler(txt188_Change);
            TextBox137 = FindControl(FC.ControlIds.TextBox137) as TextBox;
            TextBox137.Change += new EventHandler(txt188_Change);
            TextBox194 = FindControl(FC.ControlIds.TextBox194) as TextBox;
            TextBox194.Change += new EventHandler(txt188_Change);
            TextBox206 = FindControl(FC.ControlIds.TextBox206) as TextBox;
            TextBox206.Change += new EventHandler(txt188_Change);

            TextBox190 = FindControl(FC.ControlIds.TextBox190) as TextBox;
            TextBox190.Change += new EventHandler(txt190_Change);
            TextBox148 = FindControl(FC.ControlIds.TextBox148) as TextBox;
            TextBox148.Change += new EventHandler(txt190_Change);
            TextBox150 = FindControl(FC.ControlIds.TextBox150) as TextBox;
            TextBox150.Change += new EventHandler(txt190_Change);
            TextBox152 = FindControl(FC.ControlIds.TextBox152) as TextBox;
            TextBox152.Change += new EventHandler(txt190_Change);
            TextBox154 = FindControl(FC.ControlIds.TextBox154) as TextBox;
            TextBox154.Change += new EventHandler(txt190_Change);
            TextBox156 = FindControl(FC.ControlIds.TextBox156) as TextBox;
            TextBox156.Change += new EventHandler(txt190_Change);
            TextBox158 = FindControl(FC.ControlIds.TextBox158) as TextBox;
            TextBox158.Change += new EventHandler(txt190_Change);
            TextBox160 = FindControl(FC.ControlIds.TextBox160) as TextBox;
            TextBox160.Change += new EventHandler(txt190_Change);
            TextBox162 = FindControl(FC.ControlIds.TextBox162) as TextBox;
            TextBox162.Change += new EventHandler(txt190_Change);
            TextBox164 = FindControl(FC.ControlIds.TextBox164) as TextBox;
            TextBox164.Change += new EventHandler(txt190_Change);
            TextBox166 = FindControl(FC.ControlIds.TextBox166) as TextBox;
            TextBox166.Change += new EventHandler(txt190_Change);
            TextBox196 = FindControl(FC.ControlIds.TextBox196) as TextBox;
            TextBox196.Change += new EventHandler(txt190_Change);
            TextBox208 = FindControl(FC.ControlIds.TextBox208) as TextBox;
            TextBox208.Change += new EventHandler(txt190_Change);
            DropdownBox7 = FindControl(FC.ControlIds.DropdownBox7) as DropdownBox;
            // DropdownBox7.Change += new EventHandler(dd7_change);
            DropdownBox8 = FindControl(FC.ControlIds.DropdownBox8) as DropdownBox;
            // DropdownBox8.Change += new EventHandler(dd8_change);
            DropdownBox9 = FindControl(FC.ControlIds.DropdownBox9) as DropdownBox;
            //  DropdownBox9.Change += new EventHandler(dd9_change);
            DropdownBox10 = FindControl(FC.ControlIds.DropdownBox10) as DropdownBox;
            // DropdownBox10.Change += new EventHandler(dd10_change);

            TextBox101 = FindControl(FC.ControlIds.TextBox101) as TextBox;
            TextBox103 = FindControl(FC.ControlIds.TextBox103) as TextBox;
            TextBox104 = FindControl(FC.ControlIds.TextBox104) as TextBox;
            TextBox110 = FindControl(FC.ControlIds.TextBox110) as TextBox;
            TextBox113 = FindControl(FC.ControlIds.TextBox113) as TextBox;

        }

        private void dd10_change(object sender, EventArgs e)
        {
            CalculateA14();
            CalculateB14();
            CalculateD14();
        }

        private void dd9_change(object sender, EventArgs e)
        {
            CalculateA14();
            CalculateB14();
            CalculateD14();
        }

        private void dd8_change(object sender, EventArgs e)
        {
            CalculateA14();
            CalculateB14();
            CalculateD14();
        }

        private void dd7_change(object sender, EventArgs e)
        {
            CalculateA14();
            CalculateB14();
            CalculateD14();
            //decimal a6Value = 0;
            //decimal d6Value = 0;

            //if (EncompassApplication.CurrentLoan.Fields["CX.RM.ED.DD1"].FormattedValue == "Monthly Interest Earned(30 Day Month)")
            //{

            //    a6Value = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].FormattedValue) / 365) *
            //        (30 * Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.BALB5"].FormattedValue));

            //    d6Value = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].FormattedValue) / 365) *
            //                    (30 * (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.BAL.E5"].FormattedValue)));
            //}
            //else if (EncompassApplication.CurrentLoan.Fields["CX.RM.ED.DD1"].FormattedValue == "Monthly Interest Earned(31 Day Month)")
            //{
            //    a6Value = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].FormattedValue) / 365) *
            //        (31 * (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.BALB5"].FormattedValue)));

            //    d6Value = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].FormattedValue) / 365) *
            //                   (31 * (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.BAL.E5"].FormattedValue)));
            //}
            //else if (EncompassApplication.CurrentLoan.Fields["CX.RM.ED.DD1"].FormattedValue == "Monthly Interest Earned(29 Day Month)")
            //{
            //    a6Value = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].FormattedValue) / 365) *
            //        (29 * (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.BALB5"].FormattedValue)));

            //    d6Value = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].FormattedValue) / 365) *
            //                   (29 * (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.BAL.E5"].FormattedValue)));
            //}
            //else if (EncompassApplication.CurrentLoan.Fields["CX.RM.ED.DD1"].FormattedValue == "Monthly Interest Earned(28 Day Month)")
            //{
            //    a6Value = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].FormattedValue) / 365) *
            //        (28 * (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.BALB5"].FormattedValue)));

            //    d6Value = (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.ANNUALINTRATE"].FormattedValue) / 365) *
            //                   (28 * (Convert.ToDecimal(EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.BAL.E5"].FormattedValue)));
            //}
            //EncompassApplication.CurrentLoan.Fields["CX.RM.ED.EA.A6"].Value = a6Value.ToString();
            //EncompassApplication.CurrentLoan.Fields["CX.RM.ED.RA.D6"].Value = d6Value.ToString();
        }

        private void txt190_Change(object sender, EventArgs e)
        {
            CalculateD14();
        }

        private void txt188_Change(object sender, EventArgs e)
        {
            CalculateA14();
            CalculateB14();
        }
    }
}
