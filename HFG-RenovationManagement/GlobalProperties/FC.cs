﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HFG_RenovationManagement.GlobalProperties
{
    public partial class FC
    {
        public class ControlIds
        {
            public const string CBHCF = "CBHCF";
            public const string CBAF = "CBAF";
            public const string CBPERMIT = "CBPERMIT";

            public const string TextBox188 = "TextBox188";
            public const string TextBox128 = "TextBox128";
            public const string TextBox129 = "TextBox129";
            public const string TextBox130 = "TextBox130";
            public const string TextBox131 = "TextBox131";
            public const string TextBox132 = "TextBox132";
            public const string TextBox133 = "TextBox133";
            public const string TextBox134 = "TextBox134";
            public const string TextBox135 = "TextBox135";
            public const string TextBox136 = "TextBox136";
            public const string TextBox137 = "TextBox137";
            public const string TextBox194 = "TextBox194";
            public const string TextBox206 = "TextBox206";



            public const string TextBox190 = "TextBox190";
            public const string TextBox148 = "TextBox148";
            public const string TextBox150 = "TextBox150";
            public const string TextBox152 = "TextBox152";
            public const string TextBox154 = "TextBox154";
            public const string TextBox156 = "TextBox156";
            public const string TextBox158 = "TextBox158";
            public const string TextBox160 = "TextBox160";
            public const string TextBox162 = "TextBox162";
            public const string TextBox164 = "TextBox164";
            public const string TextBox166 = "TextBox166";
            public const string TextBox196 = "TextBox196";
            public const string TextBox208 = "TextBox208";

            public const string DropdownBox7 = "DropdownBox7";
            public const string DropdownBox8 = "DropdownBox8";
            public const string DropdownBox9 = "DropdownBox9";
            public const string DropdownBox10 = "DropdownBox10";


            public const string TextBox101 = "TextBox101";
            public const string TextBox103 = "TextBox103";
            public const string TextBox104 = "TextBox104";
            public const string TextBox110 = "TextBox110";
            public const string TextBox113 = "TextBox113";
        }
    }
}
